package com.innside.steve.email;

import java.io.IOException;

import com.innside.steve.model.EmailRegister;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

public class SendEmail {
	
	public void sendEmail(EmailRegister emailRegister) throws IOException{
		Email from = new Email(emailRegister.getSender());
	    String subject = emailRegister.getSubject();
	    Email to = new Email(emailRegister.getReceiver());
	    Content content = new Content("text/plain", emailRegister.getContent());
	    Mail mail = new Mail(from, subject, to, content);
	    SendGrid sg = new SendGrid(emailRegister.getApiKey());
	    Request request = new Request();
	    try {
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
	      System.out.println(response.getStatusCode());
	      System.out.println(response.getBody());
	      System.out.println(response.getHeaders());
	    } catch (IOException ex) {
	      throw ex;
	    }
	}

}
